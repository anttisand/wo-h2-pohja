<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require "App.php";
require "Task.php";
require "database/connection.php";
require "database/QueryBuilder.php";

App::bind('config', require 'config.php');

App::bind('database', new QueryBuilder(
	Connection::make(App::get('config')['database'])
));

App::get('database')
	->query('INSERT INTO todo (id, description, completed) VALUES (
		default, :description, :completed)')
	->bind(':description', 'My new task')
	->bind(':completed', false)
	->execute();

$myTodos = App::get('database')
				->query('SELECT * FROM todo')
				->getAll('Task');

require 'resources/views/index.view.php';