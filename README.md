# WWW-ohjelmointi 2017

## Tilanne kolmannen luennon kohdalla

Tätä pohjaa voi käyttää harjoituksen 2 taustalla. Tätä ei tule käyttää missään
oikeassa projektissa, sillä tähän joudumme vielä refaktoroimaan useita 
tietoturva paikkauksia. Ja lisää toiminnallisuutta.

*HUOM* Tietokantayhteyden tunnukset määrittävä config.php on tässä mukana 
esimerkin vuoksi. Yleensä tällaiset asetustiedostot eivät kuulu versiohallintaan.
Versiohallintaan voidaan laittaa asetustiedostopohja, jossa ei ole täytettynä
tunnuksia, esimerkiksi config.example.php.

Voit poistaa asetustiedoston versionhallinasta Git -versionhallinnassa lisäämällä
hakemistoon tiedoston .gitignore ja listaamalla sinne tiedostot, jotka haluat
versionhallinnan jättävän lisäämättä.